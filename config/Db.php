<?php

namespace Config;

use Exception;
use mysqli;

class Db
{
    private $host = 'localhost';
    private $databasename = 'liteforex-final';
    private $username = 'root';
    private $password = '';
    private $connection = null;

    public function connect()
    {
        try {
            $this->connection = new mysqli($this->host, $this->username, $this->password, $this->databasename);
        } catch (Exception $e) {
            //throw $th;
            echo $this->connection->connect_error;
        }

        return $this->connection;
    }
}
