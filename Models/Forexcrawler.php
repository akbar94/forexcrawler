<?php

namespace Models;
require __DIR__ . '/../vendor/autoload.php';

use Config\Db;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Exception\GuzzleException as GuzzleException;
use GuzzleHttp\Exception\ConnectException as ConnectException;

class Forexcrawler
{
    private $client;
    private $crawlUserId;
    private $crawl;
    private $currentpagenumber;

    public function __construct()
    {

        $this->client = new Client([
            'timeout' => 10,
            'verify' => false
        ]);
    }

    /**
     * crawl and save traders to db
     * @param void
     * @return void
     */
    public function saveTraders()
    {
        $pageNumber = 1;
        $j = 1;
        while ($j === 1) {
            try {
                $response = $this->client->get('https://my.liteforex.com/traders/load?type=total_equity&filter%5Bprofit%5D%5Bfrom%5D=0&filter%5Bdays%5D%5Bfrom%5D=10&filter%5Binvestors_count%5D%5Bfrom%5D=5&filter%5Bprofit_sharing%5D%5Bto%5D=50&view=list&&page=' . $pageNumber);
                // get contents and pass to the crawler
                $content = $response->getBody()->getContents();
                if (strpos($content, 'card') == false) {
                    echo "no card";
                    break;
                }
                $t = $this;
                $crawler = new Crawler($content, 'https://my.liteforex.com/traders');
                $pageNumber++;
                $crawler->filter('div.card > div.inner')->each(function (Crawler $node, $i) use ($t) {
                    $trader = new Trader();
                    $trader->setUsername($node->filter('div.data_col >  div.title')->text());
                    $trader->setPeriod(preg_replace('/[^0-9]/', '', $node->filter('div.data_col >  div.data > div.data_value')->eq(0)->text()));
                    $trader->setUserid(preg_replace('/[^0-9]/', '', $node->filter('a.link')->link()->getUri()));
                    $trader->setRisk(preg_replace('/[^0-9]/', '', $node->filter('div.data_col > div > span')->text()));
                    $trader->setProfitability($node->filter('div.data_col >  div.data > div.data_value')->eq(1)->text());
                    $trader->setCommission(preg_replace('/[^0-9]/', '', $node->filter('div.data_col >  div.data > div.data_value')->eq(2)->text()));
                    $trader->setCopying($node->filter('div.data_col >  div.data > div.data_value')->eq(3)->text());
                    $trader->setTotalAssets(preg_replace('/[^0-9]/', '', $node->filter('div.data_col >  div.data > div.data_value')->eq(4)->text()));
                    $trader->setLink($node->filter('a.link')->link()->getUri());
                    $trader->setPersonal_assets($trader->getPersonal_assets());
                    $trader->saveToDb();
                });
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
    }

    /**
     * get all traders from db , loop throw them and excute  saveTrade() on trader
     * @param
     * @return void
     *
     */
    public function crawlTrades()
    {
        echo "crawling trades started. \n";
        $file = file_get_contents('Logs/trade.json');
        $tradelog = fopen('Logs/trade.json', 'w');

        $array = json_decode($file, 1);
        $userjsonid = $array['userid'];
        $pagenumber = $array['pagenumber'];
//        $array['userid'] = null;
//        $array['pagenumber'] = null;
        $json = json_encode($array);
        fwrite($tradelog, $json);
        fclose($tradelog);
        echo $userjsonid;
        echo "\n";
        echo $pagenumber;

        $traders = [];
        $db = new Db();
        $conn = $db->connect();
        if ($userjsonid !== null) {
            //return user db ID by userid that returned from crawler intrupt
            $traderID = [];
            $useridsql = "SELECT * FROM traders where userid = $userjsonid";
            $result = $conn->query($useridsql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $traderID[] = $row;
                }
            }
            $id = (int)($traderID[0]['id']);
            $sql = "SELECT * FROM traders WHERE id >= $id ";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $traders[] = $row;
                }
            }
            foreach ($traders as $trader) {

                $fileX = file_get_contents('Logs/trade.json');
                $arrayX = json_decode($fileX, 1);
                $userjsonidX = $arrayX['userid'];
                $pagenumberX = $arrayX['pagenumber'];
                echo $pagenumberX;
                $this->saveTrade($pagenumberX, $trader['userid']);
                if ($userjsonidX == "next_user")
                    continue;
            }

        } else {

            $sql = "SELECT * FROM traders";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $traders[] = $row;
                }
            }
            foreach ($traders as $trader) {

                $this->saveTrade(1, $trader['userid']);
            }
        }
        echo "DONE";
    }


    public function saveTrade($pagenumber = 1, $userid)
    {
        $this->crawlUserId = $userid; ///// ********************* check it
        $this->currentpagenumber = $pagenumber;
        $trades = [];

        $tradelog = fopen('Logs/trade.json', 'w');
        $array = [];
        $array['userid'] = $userid;
        $array['pagenumber'] = $pagenumber;
        $json = json_encode($array);
        fwrite($tradelog, $json);
        fclose($tradelog);

        do {
            try {
                $response = $this->client->get('https://my.liteforex.com/traders/trades-history-load?id=' . $userid . '&page=' . $this->currentpagenumber);
                // get contents and pass to the crawler
                $content = $response->getBody()->getContents();
                if (strpos($content, 'content_col') == false) {

                    $tradelog = fopen('Logs/trade.json', 'w');
                    $array = [];
                    $array['userid'] = "next_user";
                    $array['pagenumber'] = 1;
                    $json = json_encode($array);
                    fwrite($tradelog, $json);
                    fclose($tradelog);
                    echo "\n breaked userID : $this->crawlUserId page: $this->currentpagenumber \n";

                    break;
                }

                $t = $this;
                $crawler = new Crawler($content);

                $data = $crawler->filter('div.content_row')->each(function (Crawler $node, $i) use ($t) {
                    echo $this->crawlUserId . "\n";
                    return $this->getTradeNodeContent($node, $this->crawlUserId);
                });


                foreach ($data as $value) {
                    array_push($trades, $value);
                }

            } catch (GuzzleException $e) {

                $tradelog = fopen('Logs/trade.json', 'w');
                $array = [];
                $array['userid'] = $this->crawlUserId;
                $array['pagenumber'] = $this->currentpagenumber;
                $json = json_encode($array);
                fwrite($tradelog, $json);
                fclose($tradelog);

                echo "connection error";
                die();
            }
            $this->currentpagenumber++;

        } while ($this->crawl <= 1);
    }


    /* *
* get trading histories node datas and insert it to db
* @param crawlerObject node
* @return array of trade details
 */
    private function getTradeNodeContent($node, $userid)
    {
        $trade = new Trade();
        $trade->setInstrumet_trader($node->filter('div.content_col >  div.title')->text());
        $trade->setTraderid($userid);
        $trade->setOpening_date($node->filter('div.content_col >  div.data > div.data_value')->eq(0)->text());
        $trade->setClosing_date($node->filter('div.content_col >  div.data > div.data_value')->eq(1)->text());
        $trade->setTrade_type($node->filter('div.content_col >  div.label ')->text());
        $trade->setTrade_volume($node->filter('div.content_col >  div.data > div.data_value')->eq(2)->text());
        $trade->setEntry_point($node->filter('div.content_col >  div.data > div.data_value')->eq(3)->text());
        $trade->setExit_point($node->filter('div.content_col >  div.data > div.data_value')->eq(4)->text());
        $trade->setProfit($node->filter('div.content_col >  div.data > div.data_value')->eq(5)->text());
        $this->saveTradeJson($this->currentpagenumber, $trade->getTraderid());
        echo $trade->getTraderid() . " " . $trade->getClosing_date() . "currentPage: $this->currentpagenumber \n ";
        //check if tradeexists jump one page
        if ($trade->checktradeExists()) {
//            $this->crawl =2;
            echo "\n" . $trade->getOpening_date() . "\n";
            echo "checktradeExists \n";
            return $trade;
        }
        $trade->insertradeToDb();
        return $trade;
    }


    // get pagenumber and user id save to trade.json file
    public function saveTradeJson($pageNumber, $userid)
    {
        $array = [];
        $file = file_get_contents('Logs/trade.json');
        $tradelog = fopen('Logs/trade.json', 'w');
        $array = json_decode($file, 1);
        $array['userid'] = $userid;
        $array['pagenumber'] = $pageNumber;
        $json = json_encode($array);
        fwrite($tradelog, $json);
        fclose($tradelog);
    }


    /* *
    * get porfolio node datas and insert it to db
    * @param $userid
    * @return array of portfolio details
     */
    public function savePortfolio($userid)
    {
        $this->crawlUserId = $userid;
        $portfolios = [];
        do {
            try {
                $response = $this->client->get('https://my.liteforex.com/traders/trades?id=' . $userid);
                // get contents and pass to the crawler
                $content = $response->getBody()->getContents();
                if (strpos($content, 'content_row') == false) {
                }
                $t = $this;
                $crawler = new Crawler($content);

                $data = $crawler->filter('div.content_row')->each(function (Crawler $node, $i) use ($t) {
                    return $this->getPortfolioNodeContent($node, $this->crawlUserId);
                });

                foreach ($data as $value) {
                    array_push($portfolios, $value);
                }
            } catch (GuzzleException $e) {
                return $this->currentpagenumber;
            }
        } while ($this->crawl == 1);

        return $portfolios;
    }

    private function getPortfolioNodeContent($node, $userid)
    {
        $portfolio = new Portfolio();
        $portfolio->setTrader_id($userid);
        $portfolio->setInstrument_trader($node->filter('div.content_col >  div.title')->text());
        $portfolio->setOpening_date($node->filter('div.content_col >  div.data > div.data_value')->eq(1)->text());
        $portfolio->setTrade_type($node->filter('div.content_col >  div.label ')->text());
        $portfolio->setTrade_volume($node->filter('div.content_col >  div.data > div.data_value')->eq(0)->text());
        $portfolio->setEntry_point($node->filter('div.content_col >  div.data > div.data_value')->eq(2)->text());
        $portfolio->setCurrent_level($node->filter('div.content_col >  div.data > div.data_value')->eq(3)->text());
        $portfolio->setStop_loss($node->filter('div.content_col >  div.data > div.data_value')->eq(4)->text());
        $portfolio->setTake_profit($node->filter('div.content_col >  div.data > div.data_value')->eq(5)->text());
        $portfolio->setProfit($node->filter('div.content_col >  div.data > div.data_value')->eq(6)->text());

        //check if tradeexists jump one page
        if ($portfolio->checkPortfolioExists()) {
            $this->crawl++;
            return $portfolio;
        }
        $portfolio->inserPortfolioToDb();
        return $portfolio;
    }

    /**
     * get all traders from db,  loop throw them and excute savePortfolio on trader
     * @param
     * @return void
     */
    public function crawlPortfolios()
    {
        echo "Portfolios Crawl Started : \n";
//        $file = file_get_contents('Logs/trade.json');
//        $portfolio = fopen('Logs/trade.json', 'w');

//        $array = json_decode($file, 1);
//        $userjsonid = $array['userid'];
//        $pagejsonnumber = $array['pagenumber'];
//        $json = json_encode($array);
//        fwrite($portfolio, $json);
//        fclose($portfolio);
//        $this->saveTradeJson('null', 'null');

        $traders = [];
        $db = new Db();
        $conn = $db->connect();
        $sql = "SELECT * FROM traders";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $traders[] = $row;
            }
        }
        foreach ($traders as $trader) {
            $this->savePortfolio($trader['userid']);
        }
        echo "Portfolio Crawl Done!";
        $this->saveTradeJson('null', 'null');
    }

    // get pagenumber and user id save to portfolio.json file
    public function savePortfolioJson($pageNumber, $userid)
    {
        $array = [];
        $file = file_get_contents('Logs/portfolio.json');
        $portfoliolog = fopen('Logs/portfolio.json', 'w');
        $array = json_decode($file, 1);
        $array['userid'] = $userid;
        $array['pagenumber'] = $pageNumber;
        $json = json_encode($array);
        fwrite($portfoliolog, $json);
        fclose($portfoliolog);
    }

}