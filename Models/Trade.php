<?php

namespace Models;

require_once __DIR__ . '/../vendor/autoload.php';

use Config\Db;

class Trade
{

    private $instrumet_trader;
    private $trader_id;
    private $opening_date;
    private $closing_date;
    private $trade_type;
    private $trade_volume;
    private $entry_point;
    private $exit_point;
    private $profit;

    public function setInstrumet_trader($instrumet_trader)
    {
        $this->instrumet_trader = $instrumet_trader;
    }

    public function getInstrumet_trader()
    {
        return $this->instrumet_trader;
    }

    public function setTraderid($trader_id)
    {
        $this->trader_id = $trader_id;
    }

    public function getTraderid()
    {
        return $this->trader_id;
    }

    public function setOpening_date($opening_date)
    {
        $this->opening_date = $opening_date;
    }

    public function getOpening_date()
    {
        return $this->opening_date;
    }

    public function setClosing_date($closing_date)
    {
        $this->closing_date = $closing_date;
    }

    public function getClosing_date()
    {
        return $this->closing_date;
    }

    public function setTrade_type($trade_type)
    {
        $this->trade_type = $trade_type;
    }

    public function getTrade_type()
    {
        return $this->trade_type;
    }

    public function setTrade_volume($trade_volume)
    {
        $this->trade_volume = $trade_volume;
    }

    public function getTrade_volume()
    {
        return $this->trade_volume;
    }

    public function setEntry_point($entry_point)
    {
        $this->entry_point = $entry_point;
    }

    public function getEntry_point()
    {
        return $this->entry_point;
    }

    public function setExit_point($exit_point)
    {
        $this->exit_point = $exit_point;
    }

    public function getExit_point()
    {
        return $this->exit_point;
    }

    public function setProfit($profit)
    {
        $this->profit = $profit;
    }

    public function getProfit()
    {
        return $this->profit;
    }

    public function insertradeToDb()
    {

        $db = new Db();
        $conn = $db->connect();

        if ($conn->query("SELECT * FROM trade_histories WHERE opening_date = '$this->opening_date' AND trader_id = '$this->trader_id' AND closing_date = '$this->closing_date'  ")->num_rows > 0) {
            $this->checkPortfolio($this->trader_id, $this->opening_date, $this->entry_point);
            $conn->close();

        } else {
            $conn->query("INSERT INTO trade_histories (trader_id, instrumet_trader, opening_date,closing_date,trade_type,trade_volume,entry_point,exit_point,profit)
            VALUES ('$this->trader_id','$this->instrumet_trader','$this->opening_date','$this->closing_date','$this->trade_type','$this->trade_volume','$this->entry_point','$this->exit_point','$this->profit' ) ");
            $conn->close();

        }
    }

    public function checktradeExists()
    {

        $db = new Db();
        $conn = $db->connect();

        if ($conn->query("SELECT * FROM trade_histories WHERE opening_date = '$this->opening_date' AND trader_id = '$this->trader_id'  AND closing_date = '$this->closing_date' ")->num_rows > 0) {
            $conn->close();
            return true;
        } else {
            $conn->close();
            return false;
        }
    }

    public function checkPortfolio($traderid, $openingdate, $entrypoint)
    {
        $data = [];
        $db = new Db();
        $conn = $db->connect();
        $res = $conn->query("select * from portfolios where trader_id = $traderid AND opening_date = '$openingdate' AND entry_point = '$entrypoint'");
        if ($res->num_rows > 0) {
            while ($row = $res->fetch_assoc()) {
                $data = $row;
            }
            $id = $data['id'];
            $conn->query("update portfolios set status = 1 where id = $id ");
            $conn->close();
            return true;
        }
        return false;
    }
}
