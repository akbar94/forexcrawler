<?php

namespace Models;

require_once __DIR__ . '/../vendor/autoload.php';

use Config\Db;

class Portfolio
{
    private $trader_id;
    private $instrumet_trader;
    private $opening_date;
    private $trade_type;
    private $trade_volume;
    private $entry_point;
    private $current_level;
    private $stop_loss;
    private $take_profit;
    private $profit;
    private $status;

    /* *
    * Set trader_id 
    * @param string trader_id
     */
    public function setTrader_id($trader_id)
    {
        $this->trader_id = $trader_id;
    }
    /* *
    * return trader_id 
    * @return int trader_id
     */
    public function getTrader_id()
    {
        return $this->trader_id;
    }

    /* *
    * Set instrument_trader 
    * @param string instrument_trader
     */
    public function setInstrument_trader($instrumet_trader)
    {
        $this->instrumet_trader = $instrumet_trader;
    }
    /* *
    * return insturment_trader 
    * @return string instrument_trader
     */
    public function getInstrument_trader()
    {
        return $this->instrumet_trader;
    }
    /* *
    * Set opening_date 
    * @param string opening_date
     */
    public function setOpening_date($opening_date)
    {
        $this->opening_date = $opening_date;
    }
    /* *
    * return opening_date   
    * @param string opening_date
     */
    public function getOpening_date()
    {
        return $this->opening_date;
    }
    /* *
    * Set trade_type 
    * @param string trade_type
     */
    public function setTrade_type($trade_type)
    {
        $this->trade_type = $trade_type;
    }
    /* *
    * return trade_type 
    * @param string trade_type
     */
    public function getTrade_type()
    {
        return $this->trade_type;
    }
    /* *
    * Set trade_volume   
    * @param string trade_volume
     */
    public function setTrade_volume($trade_volume)
    {
        $this->trade_volume = $trade_volume;
    }
    /* *
    * return trade_volume 
    * @param string trader_volume
     */
    public function getTrade_volume()
    {
        return $this->trade_volume;
    }
    /* *
    * Set entry_point 
    * @param float entry_point
     */
    public function setEntry_point($entry_point)
    {
        $this->entry_point = $entry_point;
    }
    /* *
    * return  entry_point 
    * @param float entry_point
     */
    public function getEntry_point()
    {
        return $this->entry_point;
    }
    /* *
    * Set current_level 
    * @param float current_level
     */
    public function setCurrent_level($current_level)
    {
        $this->current_level = $current_level;
    }
    /* *
    * return  current_level 
    * @param float current_level
     */
    public function getCurrent_level()
    {
        return $this->current_level;
    }
    /* *
    * Set stop_loss 
    * @param float stop_loss
     */
    public function setStop_loss($stop_loss)
    {
        $this->stop_loss = $stop_loss;
    }
    /* *
    * return stop_loss 
    * @param float stop_loss
     */
    public function getStop_loss()
    {
        return $this->stop_loss;
    }
    /* *
    * Set take_profit 
    * @param float take_profit
     */
    public function setTake_profit($take_profit)
    {
        $this->take_profit = $take_profit;
    }
    /* *
    * return take_profit 
    * @param float take_profit
     */
    public function getTake_profit()
    {
        return $this->take_profit;
    }
    /* *
    * Set profit 
    * @param float profit
     */
    public function setProfit($profit)
    {
        $this->profit = $profit;
    }
    /* *
    * return  profit 
    * @param float profit
     */
    public function getProfit()
    {
        return $this->profit;
    }
    /* *
    * Set status
    * @param bit status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    /* *
    * return  profit
    * @param float profit
     */
    public function getStatus()
    {
        return $this->status;
    }
    public function inserPortfolioToDb()
    {
        $db = new Db();
        $conn = $db->connect();
        $result =  $conn->query("SELECT * FROM portfolios WHERE opening_date = '$this->opening_date' AND trader_id = '$this->trader_id'");
        if ($result->num_rows > 0) {
            $conn->close();
        } else {
            $conn->query("INSERT INTO portfolios (trader_id, instrumet_trader, opening_date,trade_type,trade_volume,entry_point,profit,current_level,stop_loss,take_profit)
            VALUES ('$this->trader_id','$this->instrumet_trader','$this->opening_date','$this->trade_type','$this->trade_volume','$this->entry_point','$this->profit',$this->current_level, $this->stop_loss,$this->take_profit) ");
            $conn->close();

        }
    }
    public function checkPortfolioExists()
    {
        $db = new Db();
        $conn = $db->connect();
        $result =  $conn->query("SELECT * FROM portfolios WHERE opening_date = '$this->opening_date' AND trader_id = '$this->trader_id'");
        if ($result->num_rows > 0) {
            $conn->close();
            return true;
        } else {
            return false;
        }
    }
}
