<?php

namespace Models;

require __DIR__ . '/../vendor/autoload.php';

use GuzzleHttp as GuzzleHttp;
use Symfony\Component\DomCrawler as DomCrawler;
use Config\Db;


class Trader
{
    private $username;
    private $userid;
    private $userdbid;
    private $period;
    private $risk;
    private $profitability;
    private $commission;
    private $copying;
    private $totalAssets;
    private $link;
    private $client;
    private $currentpagenumber;
    private $personalassets;

    private $crawl = 1;

    function __construct__()
    {
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }
    public function getUsername()
    {
        return $this->username;
    }
    public function setUserid($userid)
    {
        $this->userid = $userid;
    }
    public function getUserid()
    {
        return $this->userid;
    }
    public function setUserdbid($userdbid)
    {
        $this->userdbid = $userdbid;
    }
    public function getUserdbid()
    {
        return $this->userdbid;
    }
    public function setPeriod($period)
    {
        $this->period = $period;
    }
    public function getPeriod()
    {
        return $this->period;
    }
    public function setRisk($risk)
    {
        $this->risk = $risk;
    }
    public function getRisk()
    {
        return $this->risk;
    }
    public function setProfitability($profitability)
    {
        $this->profitability = $profitability;
    }
    public function getProfitability()
    {
        return $this->profitability;
    }
    public function setCommission($commission)
    {
        $this->commission = $commission;
    }
    public function getCommission()
    {
        return $this->commission;
    }
    public function setCopying($copying)
    {
        $this->copying = $copying;
    }
    public function getCopying()
    {
        return $this->copying;
    }
    public function setTotalAssets($totalAssets)
    {
        $this->totalAssets = $totalAssets;
    }
    public function getTotalAssets()
    {
        return $this->totalAssets;
    }
    public function setLink($link)
    {
        $this->link = $link;
    }
    public function getLink()
    {
        return $this->link;
    }
    /**
     * save trader object data to dabase
     *
     */
    public function saveToDb()
    {
        $db = new Db();
        $conn = $db->connect();
        $checkTrader = $conn->query("SELECT * FROM traders WHERE username = '$this->username'");
        if ($checkTrader->num_rows > 0) {
            $checkTrader = $conn->query("SELECT * FROM traders WHERE username = '$this->username'");
            $sql = "UPDATE traders SET  period = '$this->period', risk = '$this->risk' ,profitability = '$this->profitability' ,commission = '$this->commission' ,copying = '$this->copying',total_assets = '$this->totalAssets' ,personal_assets = '$this->personalassets' WHERE username = '$this->username' ";
            // $conn->query($sql);
            if ($conn->query($sql) === TRUE) {
                echo " record updated successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        } else {
            $sql = "INSERT INTO traders (username ,userid, period, risk ,profitability ,commission,copying,total_assets,link,personal_assets) VALUES('$this->username','$this->userid','$this->period','$this->risk','$this->profitability','$this->commission','$this->copying','$this->totalAssets','$this->link','$this->personalassets') ";
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        }

        $conn->close();
    }
    /**
     * set personal asset to trader obj
     * @param $personal_assets String
     * @return void
     */
    public function setPersonal_assets($personal_assets)
    {
        $this->personalassets = $personal_assets;
    }
    /**
     * crawl personal assets of user
     * @param void
     * @return string $personal_assets
     */
    public function getPersonal_assets()
    {
        $client =  new GuzzleHttp\Client([
            'timeout' => 10,
            'verify' => false
        ]);

        $personalassetresponse = $client->get("https://my.liteforex.com/traders/info?id=" . $this->userid);
        $content = $personalassetresponse->getBody()->getContents();

        $crawler = new DomCrawler\Crawler($content);
        $personalassets = $crawler->filter('div.panel_inner > div.data > div.data_value ')->eq(2)->text();
        return (string)($personalassets);
    }
}
